package nix.training.auto.spec

import geb.spock.GebReportingSpec
import nix.training.auto.page.StartPage
import nix.training.auto.page.BlogPage


class NixNavigationSpec extends GebReportingSpec {

    def "Navigate to Main page"(){
        when:
            to StartPage

        and:
            "User navigates to Blog page"()

        then:
            at BlogPage
    }
}
